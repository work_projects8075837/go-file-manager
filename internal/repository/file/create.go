package repository

import (
	"context"
	domain "file-com/internal/domain/file"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (r FileRepo) Create(ctx context.Context, f *domain.File) error {
	modelFile := convertDomainFileToFileModel(f)

	tx, _ := boil.BeginTx(ctx, nil)

	err := modelFile.Insert(ctx, tx, boil.Infer())

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return err
}
