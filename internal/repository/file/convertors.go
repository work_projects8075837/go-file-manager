package repository

import (
	"file-com/database/models"
	domain "file-com/internal/domain/file"
	"github.com/volatiletech/null/v8"
)

func convertDomainFileToFileModel(f *domain.File) models.File {
	return models.File{
		ID:         f.Id,
		Name:       f.Name,
		Path:       f.Path,
		CreatedAt:  f.CreatedAt,
		UpdatedAt:  f.UpdatedAt,
		Size:       null.IntFrom(f.Size),
		IsRemoving: f.IsRemoving,
	}
}

func convertFileModelToDomainFile(f *models.File) domain.File {
	return domain.File{
		Id:         f.ID,
		Name:       f.Name,
		Path:       f.Path,
		CreatedAt:  f.CreatedAt,
		UpdatedAt:  f.UpdatedAt,
		Size:       f.Size.Int,
		IsRemoving: f.IsRemoving,
	}
}
