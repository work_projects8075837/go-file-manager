package fileApp

import (
	"context"
	controller "file-com/internal/controller/file"
	repository "file-com/internal/repository/file"
	usecase "file-com/internal/usecase/file"
	"github.com/gofiber/fiber/v2"
)

func SetupFileRoutes(ctx context.Context, router fiber.Router) {

	fileController := controller.NewFileController(*usecase.NewFileUseCase(*repository.NewFileRepo()))

	filesRouter := router.Group("/file")

	filesRouter.Post("/", func(fiberCtx *fiber.Ctx) error {
		return fileController.Create(ctx, fiberCtx)
	})

}
