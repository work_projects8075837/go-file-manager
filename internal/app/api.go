package app

import (
	"context"
	"file-com/config"
	"file-com/database"
	fileApp "file-com/internal/app/file"
	"fmt"
	"github.com/gofiber/contrib/swagger"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"os"
)

// NewRouter -.
// Swagger spec:
//	@securityDefinitions.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization

func RunApi() {
	config.Init()
	database.Init()

	ctx := context.Background()

	app := fiber.New(config.GetFiberConfig())

	app.Use(cors.New())

	app.Use(swagger.New(config.SwaggerConfig))

	api := app.Group("/api-file")

	app.Static("/api-file/static", "./static", config.StaticConfig)

	fileApp.SetupFileRoutes(ctx, api)

	err := app.Listen(":" + os.Getenv("PORT"))

	if err != nil {
		fmt.Println("Error ", err)
	}
}
