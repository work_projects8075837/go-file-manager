package usecase

import (
	"context"
	domain "file-com/internal/domain/file"
	"sync"
)

func (uc *FileUseCase) InsertFiles(ctx context.Context, domainFiles []*domain.File) {
	var wg sync.WaitGroup

	wg.Add(len(domainFiles))

	for _, domainFile := range domainFiles {

		go func(ctx context.Context, wg *sync.WaitGroup, domainFile *domain.File) {
			_ = uc.Create(ctx, domainFile)
			defer wg.Done()
		}(ctx, &wg, domainFile)
	}

	go func() {
		wg.Wait()
	}()

}
