package usecase

import (
	"context"
	domain "file-com/internal/domain/file"
)

func (uc *FileUseCase) Create(ctx context.Context, f *domain.File) error {
	return uc.repo.Create(ctx, f)
}
