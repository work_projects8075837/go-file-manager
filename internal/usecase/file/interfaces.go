package usecase

import (
	"context"
	"file-com/internal/domain/file"
)

type IFileRepo interface {
	Create(ctx context.Context, f *domain.File) error
}
