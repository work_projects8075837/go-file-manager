package usecase

import repository "file-com/internal/repository/file"

type FileUseCase struct {
	repo repository.FileRepo
}

func NewFileUseCase(r repository.FileRepo) *FileUseCase {
	return &FileUseCase{
		repo: r,
	}
}
