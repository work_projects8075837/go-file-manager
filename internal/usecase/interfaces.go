package usecase

import "context"

type BaseRepo[T any, C any, U any, D any] interface {
	Create(ctx context.Context, data C) (T, error)
	Update(ctx context.Context, data C) (T, error)
	GetOne(ctx context.Context, id string) (D, error)
}
