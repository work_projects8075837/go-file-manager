package controller

import (
	domain "file-com/internal/domain/file"
	"mime/multipart"
)

type DomainMultipartFile struct {
	file          *domain.File
	multipartFile *multipart.FileHeader
}
