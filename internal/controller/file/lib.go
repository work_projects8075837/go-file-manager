package controller

import (
	"context"
	domain "file-com/internal/domain/file"
	"io"
	"mime/multipart"
)

func (c FileController) CreateFiles(ctx context.Context, multipartFiles []*multipart.FileHeader) ([]*domain.File, error) {

	domainFiles := make([]*domain.File, len(multipartFiles))
	fileReaders := make([]interface{ io.Reader }, len(multipartFiles))

	for i, multipartFile := range multipartFiles {
		domainFiles[i] = domain.NewFile(multipartFile.Filename, int(multipartFile.Size))
		fileReader, err := multipartFile.Open()
		if err != nil {
			return nil, err
		}
		fileReaders[i] = fileReader
	}

	saveFilesDone := make(chan struct{})
	insertFilesDone := make(chan struct{})

	go func() {
		defer close(saveFilesDone)
		domain.SaveFiles(domainFiles, fileReaders)
	}()

	go func() {
		defer close(insertFilesDone)
		c.uc.InsertFiles(ctx, domainFiles)
	}()

	<-saveFilesDone
	<-insertFilesDone

	return domainFiles, nil
}
