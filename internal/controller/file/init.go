package controller

import (
	usecase "file-com/internal/usecase/file"
)

type FileController struct {
	uc usecase.FileUseCase
}

func NewFileController(uc usecase.FileUseCase) *FileController {

	return &FileController{uc: uc}

}
