package controller

import (
	"context"
	_ "file-com/internal/domain/file"
	"fmt"
	"github.com/gofiber/fiber/v2"
	_ "net/http"
)

// @Summary		Create Array of Files
// @Description	Create Array of Files
// @Tags			file
// @Accept			json
// @Produce		json
// @Success		200	{object}	[]domain.File
// @Failure		422	{object}	NoFilesError
// @Failure		401	string		string
// @Router			/api-file/file/ [post]
func (c FileController) Create(ctx context.Context, fiberCtx *fiber.Ctx) error {

	form, err := fiberCtx.MultipartForm()

	if err != nil {
		fmt.Println(err)

		return err
	}

	multipartFiles := form.File["files"]

	if len(multipartFiles) == 0 {
		return fiberCtx.Status(422).JSON(NoFilesError{Files: "files array field is required"})
	}

	files, err := c.CreateFiles(ctx, multipartFiles)

	if err != nil {
		return err
	}

	return fiberCtx.Status(200).JSON(files)
}

type NoFilesError struct {
	Files string `json:"files"`
}
