package domain

import (
	"strings"
)

func (f *File) GetFileName() string {
	splitPath := strings.Split(f.Path, "/")
	filename := splitPath[len(splitPath)-1]
	return filename
}
