package domain

import (
	"fmt"
	"github.com/google/uuid"
	"os"
	"strings"
	"time"
)

func NewFile(name string, size int) *File {
	baseUrl := os.Getenv("BASE_URL")
	splitFilename := strings.Split(name, ".")
	ext := splitFilename[len(splitFilename)-1]

	return &File{
		Id:         uuid.New().String(),
		Name:       name,
		Path:       fmt.Sprintf("%s/api-file/static/files/%s.%s", baseUrl, uuid.New().String(), ext),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Size:       size,
		IsRemoving: false,
	}
}
