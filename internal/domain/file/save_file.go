package domain

import (
	"file-com/pkg/fileslib"
	"io"
)

func (f *File) SaveFile(imageReader interface{ io.Reader }) error {

	return fileslib.CompressAndSaveImage(imageReader, f.GetFileName())
}
