package domain

import (
	"time"
)

type File struct {
	Id         string    `json:"id"`
	Name       string    `json:"name"`
	Path       string    `json:"path"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Size       int       `json:"size"`
	IsRemoving bool      `json:"is_removing"`
}

type FileCreateDto struct {
	Name string `json:"name"`
	Path string `json:"path"`
	Size int    `json:"size"`
}
