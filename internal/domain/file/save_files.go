package domain

import (
	"io"
	"sync"
)

func SaveFiles(domainFiles []*File, fileReaders []interface{ io.Reader }) {
	var wg sync.WaitGroup
	wg.Add(len(domainFiles))

	for i, domainFile := range domainFiles {
		go func(wg *sync.WaitGroup, domainFile *File, fileReader interface{ io.Reader }) {
			domainFile.SaveFile(fileReader)
			defer wg.Done()
		}(&wg, domainFile, fileReaders[i])
	}

	wg.Wait()

}
