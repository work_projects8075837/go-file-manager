package domain

func VerifyRefresh(refresh string) (Token, error) {
	return verifyToken("REFRESH_SECRET", refresh)
}
