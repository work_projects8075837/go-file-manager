package domain

import (
	"time"
)

func CreateAccess(userId string) (string, error) {
	token, err := createToken("ACCESS_SECRET", time.Minute*15, userId)

	if err != nil {
		return "", err
	}

	return token, err
}
