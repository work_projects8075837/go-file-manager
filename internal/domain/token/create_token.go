package domain

import (
	"github.com/golang-jwt/jwt/v5"
	"os"
	"time"
)

func createToken(secretKeyName string, duration time.Duration, userId string) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.RegisteredClaims{
			Subject:   userId,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(duration)),
		})

	secretKeyString := os.Getenv(secretKeyName)

	secretKey := []byte(secretKeyString)

	tokenString, err := token.SignedString(secretKey)

	if err != nil {
		return "", err
	}
	return tokenString, err
}
