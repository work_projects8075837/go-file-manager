package domain

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"os"
)

func verifyToken(secretKeyName string, tokenString string) (Token, error) {
	secretKeyString := os.Getenv(secretKeyName)

	secretKey := []byte(secretKeyString)

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {

		return secretKey, nil
	})

	if err != nil {
		return Token{}, err
	}

	fmt.Println(token.Raw)
	if !token.Valid {
		return Token{}, errors.New("Invalid token")
	}

	claims, ok := token.Claims.(Token)

	if !ok {
		return Token{}, errors.New("Invalid token")

	}

	return claims, nil
}
