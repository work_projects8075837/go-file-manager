package domain

import "github.com/golang-jwt/jwt/v5"

type Token struct {
	jwt.Claims
	//Type  string `json:"type"`
	//Fresh bool   `json:"fresh"`
}
