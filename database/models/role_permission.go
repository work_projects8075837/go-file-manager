// Code generated by SQLBoiler 4.16.2 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/friendsofgo/errors"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"github.com/volatiletech/sqlboiler/v4/queries/qmhelper"
	"github.com/volatiletech/strmangle"
)

// RolePermission is an object representing the database table.
type RolePermission struct {
	RoleID       string    `boil:"role_id" json:"role_id" toml:"role_id" yaml:"role_id"`
	PermissionID string    `boil:"permission_id" json:"permission_id" toml:"permission_id" yaml:"permission_id"`
	ID           string    `boil:"id" json:"id" toml:"id" yaml:"id"`
	CreatedAt    time.Time `boil:"created_at" json:"created_at" toml:"created_at" yaml:"created_at"`
	UpdatedAt    time.Time `boil:"updated_at" json:"updated_at" toml:"updated_at" yaml:"updated_at"`
	IsRemoving   bool      `boil:"is_removing" json:"is_removing" toml:"is_removing" yaml:"is_removing"`

	R *rolePermissionR `boil:"-" json:"-" toml:"-" yaml:"-"`
	L rolePermissionL  `boil:"-" json:"-" toml:"-" yaml:"-"`
}

var RolePermissionColumns = struct {
	RoleID       string
	PermissionID string
	ID           string
	CreatedAt    string
	UpdatedAt    string
	IsRemoving   string
}{
	RoleID:       "role_id",
	PermissionID: "permission_id",
	ID:           "id",
	CreatedAt:    "created_at",
	UpdatedAt:    "updated_at",
	IsRemoving:   "is_removing",
}

var RolePermissionTableColumns = struct {
	RoleID       string
	PermissionID string
	ID           string
	CreatedAt    string
	UpdatedAt    string
	IsRemoving   string
}{
	RoleID:       "role_permission.role_id",
	PermissionID: "role_permission.permission_id",
	ID:           "role_permission.id",
	CreatedAt:    "role_permission.created_at",
	UpdatedAt:    "role_permission.updated_at",
	IsRemoving:   "role_permission.is_removing",
}

// Generated where

var RolePermissionWhere = struct {
	RoleID       whereHelperstring
	PermissionID whereHelperstring
	ID           whereHelperstring
	CreatedAt    whereHelpertime_Time
	UpdatedAt    whereHelpertime_Time
	IsRemoving   whereHelperbool
}{
	RoleID:       whereHelperstring{field: "\"role_permission\".\"role_id\""},
	PermissionID: whereHelperstring{field: "\"role_permission\".\"permission_id\""},
	ID:           whereHelperstring{field: "\"role_permission\".\"id\""},
	CreatedAt:    whereHelpertime_Time{field: "\"role_permission\".\"created_at\""},
	UpdatedAt:    whereHelpertime_Time{field: "\"role_permission\".\"updated_at\""},
	IsRemoving:   whereHelperbool{field: "\"role_permission\".\"is_removing\""},
}

// RolePermissionRels is where relationship names are stored.
var RolePermissionRels = struct {
}{}

// rolePermissionR is where relationships are stored.
type rolePermissionR struct {
}

// NewStruct creates a new relationship struct
func (*rolePermissionR) NewStruct() *rolePermissionR {
	return &rolePermissionR{}
}

// rolePermissionL is where Load methods for each relationship are stored.
type rolePermissionL struct{}

var (
	rolePermissionAllColumns            = []string{"role_id", "permission_id", "id", "created_at", "updated_at", "is_removing"}
	rolePermissionColumnsWithoutDefault = []string{"role_id", "permission_id", "id", "is_removing"}
	rolePermissionColumnsWithDefault    = []string{"created_at", "updated_at"}
	rolePermissionPrimaryKeyColumns     = []string{"role_id", "permission_id", "id"}
	rolePermissionGeneratedColumns      = []string{}
)

type (
	// RolePermissionSlice is an alias for a slice of pointers to RolePermission.
	// This should almost always be used instead of []RolePermission.
	RolePermissionSlice []*RolePermission
	// RolePermissionHook is the signature for custom RolePermission hook methods
	RolePermissionHook func(context.Context, boil.ContextExecutor, *RolePermission) error

	rolePermissionQuery struct {
		*queries.Query
	}
)

// Cache for insert, update and upsert
var (
	rolePermissionType                 = reflect.TypeOf(&RolePermission{})
	rolePermissionMapping              = queries.MakeStructMapping(rolePermissionType)
	rolePermissionPrimaryKeyMapping, _ = queries.BindMapping(rolePermissionType, rolePermissionMapping, rolePermissionPrimaryKeyColumns)
	rolePermissionInsertCacheMut       sync.RWMutex
	rolePermissionInsertCache          = make(map[string]insertCache)
	rolePermissionUpdateCacheMut       sync.RWMutex
	rolePermissionUpdateCache          = make(map[string]updateCache)
	rolePermissionUpsertCacheMut       sync.RWMutex
	rolePermissionUpsertCache          = make(map[string]insertCache)
)

var (
	// Force time package dependency for automated UpdatedAt/CreatedAt.
	_ = time.Second
	// Force qmhelper dependency for where clause generation (which doesn't
	// always happen)
	_ = qmhelper.Where
)

var rolePermissionAfterSelectMu sync.Mutex
var rolePermissionAfterSelectHooks []RolePermissionHook

var rolePermissionBeforeInsertMu sync.Mutex
var rolePermissionBeforeInsertHooks []RolePermissionHook
var rolePermissionAfterInsertMu sync.Mutex
var rolePermissionAfterInsertHooks []RolePermissionHook

var rolePermissionBeforeUpdateMu sync.Mutex
var rolePermissionBeforeUpdateHooks []RolePermissionHook
var rolePermissionAfterUpdateMu sync.Mutex
var rolePermissionAfterUpdateHooks []RolePermissionHook

var rolePermissionBeforeDeleteMu sync.Mutex
var rolePermissionBeforeDeleteHooks []RolePermissionHook
var rolePermissionAfterDeleteMu sync.Mutex
var rolePermissionAfterDeleteHooks []RolePermissionHook

var rolePermissionBeforeUpsertMu sync.Mutex
var rolePermissionBeforeUpsertHooks []RolePermissionHook
var rolePermissionAfterUpsertMu sync.Mutex
var rolePermissionAfterUpsertHooks []RolePermissionHook

// doAfterSelectHooks executes all "after Select" hooks.
func (o *RolePermission) doAfterSelectHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionAfterSelectHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeInsertHooks executes all "before insert" hooks.
func (o *RolePermission) doBeforeInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionBeforeInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterInsertHooks executes all "after Insert" hooks.
func (o *RolePermission) doAfterInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionAfterInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpdateHooks executes all "before Update" hooks.
func (o *RolePermission) doBeforeUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionBeforeUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpdateHooks executes all "after Update" hooks.
func (o *RolePermission) doAfterUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionAfterUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeDeleteHooks executes all "before Delete" hooks.
func (o *RolePermission) doBeforeDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionBeforeDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterDeleteHooks executes all "after Delete" hooks.
func (o *RolePermission) doAfterDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionAfterDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpsertHooks executes all "before Upsert" hooks.
func (o *RolePermission) doBeforeUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionBeforeUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpsertHooks executes all "after Upsert" hooks.
func (o *RolePermission) doAfterUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range rolePermissionAfterUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// AddRolePermissionHook registers your hook function for all future operations.
func AddRolePermissionHook(hookPoint boil.HookPoint, rolePermissionHook RolePermissionHook) {
	switch hookPoint {
	case boil.AfterSelectHook:
		rolePermissionAfterSelectMu.Lock()
		rolePermissionAfterSelectHooks = append(rolePermissionAfterSelectHooks, rolePermissionHook)
		rolePermissionAfterSelectMu.Unlock()
	case boil.BeforeInsertHook:
		rolePermissionBeforeInsertMu.Lock()
		rolePermissionBeforeInsertHooks = append(rolePermissionBeforeInsertHooks, rolePermissionHook)
		rolePermissionBeforeInsertMu.Unlock()
	case boil.AfterInsertHook:
		rolePermissionAfterInsertMu.Lock()
		rolePermissionAfterInsertHooks = append(rolePermissionAfterInsertHooks, rolePermissionHook)
		rolePermissionAfterInsertMu.Unlock()
	case boil.BeforeUpdateHook:
		rolePermissionBeforeUpdateMu.Lock()
		rolePermissionBeforeUpdateHooks = append(rolePermissionBeforeUpdateHooks, rolePermissionHook)
		rolePermissionBeforeUpdateMu.Unlock()
	case boil.AfterUpdateHook:
		rolePermissionAfterUpdateMu.Lock()
		rolePermissionAfterUpdateHooks = append(rolePermissionAfterUpdateHooks, rolePermissionHook)
		rolePermissionAfterUpdateMu.Unlock()
	case boil.BeforeDeleteHook:
		rolePermissionBeforeDeleteMu.Lock()
		rolePermissionBeforeDeleteHooks = append(rolePermissionBeforeDeleteHooks, rolePermissionHook)
		rolePermissionBeforeDeleteMu.Unlock()
	case boil.AfterDeleteHook:
		rolePermissionAfterDeleteMu.Lock()
		rolePermissionAfterDeleteHooks = append(rolePermissionAfterDeleteHooks, rolePermissionHook)
		rolePermissionAfterDeleteMu.Unlock()
	case boil.BeforeUpsertHook:
		rolePermissionBeforeUpsertMu.Lock()
		rolePermissionBeforeUpsertHooks = append(rolePermissionBeforeUpsertHooks, rolePermissionHook)
		rolePermissionBeforeUpsertMu.Unlock()
	case boil.AfterUpsertHook:
		rolePermissionAfterUpsertMu.Lock()
		rolePermissionAfterUpsertHooks = append(rolePermissionAfterUpsertHooks, rolePermissionHook)
		rolePermissionAfterUpsertMu.Unlock()
	}
}

// OneG returns a single rolePermission record from the query using the global executor.
func (q rolePermissionQuery) OneG(ctx context.Context) (*RolePermission, error) {
	return q.One(ctx, boil.GetContextDB())
}

// One returns a single rolePermission record from the query.
func (q rolePermissionQuery) One(ctx context.Context, exec boil.ContextExecutor) (*RolePermission, error) {
	o := &RolePermission{}

	queries.SetLimit(q.Query, 1)

	err := q.Bind(ctx, exec, o)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: failed to execute a one query for role_permission")
	}

	if err := o.doAfterSelectHooks(ctx, exec); err != nil {
		return o, err
	}

	return o, nil
}

// AllG returns all RolePermission records from the query using the global executor.
func (q rolePermissionQuery) AllG(ctx context.Context) (RolePermissionSlice, error) {
	return q.All(ctx, boil.GetContextDB())
}

// All returns all RolePermission records from the query.
func (q rolePermissionQuery) All(ctx context.Context, exec boil.ContextExecutor) (RolePermissionSlice, error) {
	var o []*RolePermission

	err := q.Bind(ctx, exec, &o)
	if err != nil {
		return nil, errors.Wrap(err, "models: failed to assign all query results to RolePermission slice")
	}

	if len(rolePermissionAfterSelectHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterSelectHooks(ctx, exec); err != nil {
				return o, err
			}
		}
	}

	return o, nil
}

// CountG returns the count of all RolePermission records in the query using the global executor
func (q rolePermissionQuery) CountG(ctx context.Context) (int64, error) {
	return q.Count(ctx, boil.GetContextDB())
}

// Count returns the count of all RolePermission records in the query.
func (q rolePermissionQuery) Count(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to count role_permission rows")
	}

	return count, nil
}

// ExistsG checks if the row exists in the table using the global executor.
func (q rolePermissionQuery) ExistsG(ctx context.Context) (bool, error) {
	return q.Exists(ctx, boil.GetContextDB())
}

// Exists checks if the row exists in the table.
func (q rolePermissionQuery) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)
	queries.SetLimit(q.Query, 1)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "models: failed to check if role_permission exists")
	}

	return count > 0, nil
}

// RolePermissions retrieves all the records using an executor.
func RolePermissions(mods ...qm.QueryMod) rolePermissionQuery {
	mods = append(mods, qm.From("\"role_permission\""))
	q := NewQuery(mods...)
	if len(queries.GetSelect(q)) == 0 {
		queries.SetSelect(q, []string{"\"role_permission\".*"})
	}

	return rolePermissionQuery{q}
}

// FindRolePermissionG retrieves a single record by ID.
func FindRolePermissionG(ctx context.Context, roleID string, permissionID string, iD string, selectCols ...string) (*RolePermission, error) {
	return FindRolePermission(ctx, boil.GetContextDB(), roleID, permissionID, iD, selectCols...)
}

// FindRolePermission retrieves a single record by ID with an executor.
// If selectCols is empty Find will return all columns.
func FindRolePermission(ctx context.Context, exec boil.ContextExecutor, roleID string, permissionID string, iD string, selectCols ...string) (*RolePermission, error) {
	rolePermissionObj := &RolePermission{}

	sel := "*"
	if len(selectCols) > 0 {
		sel = strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, selectCols), ",")
	}
	query := fmt.Sprintf(
		"select %s from \"role_permission\" where \"role_id\"=$1 AND \"permission_id\"=$2 AND \"id\"=$3", sel,
	)

	q := queries.Raw(query, roleID, permissionID, iD)

	err := q.Bind(ctx, exec, rolePermissionObj)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: unable to select from role_permission")
	}

	if err = rolePermissionObj.doAfterSelectHooks(ctx, exec); err != nil {
		return rolePermissionObj, err
	}

	return rolePermissionObj, nil
}

// InsertG a single record. See Insert for whitelist behavior description.
func (o *RolePermission) InsertG(ctx context.Context, columns boil.Columns) error {
	return o.Insert(ctx, boil.GetContextDB(), columns)
}

// Insert a single record using an executor.
// See boil.Columns.InsertColumnSet documentation to understand column list inference for inserts.
func (o *RolePermission) Insert(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) error {
	if o == nil {
		return errors.New("models: no role_permission provided for insertion")
	}

	var err error
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		if o.CreatedAt.IsZero() {
			o.CreatedAt = currTime
		}
		if o.UpdatedAt.IsZero() {
			o.UpdatedAt = currTime
		}
	}

	if err := o.doBeforeInsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(rolePermissionColumnsWithDefault, o)

	key := makeCacheKey(columns, nzDefaults)
	rolePermissionInsertCacheMut.RLock()
	cache, cached := rolePermissionInsertCache[key]
	rolePermissionInsertCacheMut.RUnlock()

	if !cached {
		wl, returnColumns := columns.InsertColumnSet(
			rolePermissionAllColumns,
			rolePermissionColumnsWithDefault,
			rolePermissionColumnsWithoutDefault,
			nzDefaults,
		)

		cache.valueMapping, err = queries.BindMapping(rolePermissionType, rolePermissionMapping, wl)
		if err != nil {
			return err
		}
		cache.retMapping, err = queries.BindMapping(rolePermissionType, rolePermissionMapping, returnColumns)
		if err != nil {
			return err
		}
		if len(wl) != 0 {
			cache.query = fmt.Sprintf("INSERT INTO \"role_permission\" (\"%s\") %%sVALUES (%s)%%s", strings.Join(wl, "\",\""), strmangle.Placeholders(dialect.UseIndexPlaceholders, len(wl), 1, 1))
		} else {
			cache.query = "INSERT INTO \"role_permission\" %sDEFAULT VALUES%s"
		}

		var queryOutput, queryReturning string

		if len(cache.retMapping) != 0 {
			queryReturning = fmt.Sprintf(" RETURNING \"%s\"", strings.Join(returnColumns, "\",\""))
		}

		cache.query = fmt.Sprintf(cache.query, queryOutput, queryReturning)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, vals)
	}

	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(queries.PtrsFromMapping(value, cache.retMapping)...)
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}

	if err != nil {
		return errors.Wrap(err, "models: unable to insert into role_permission")
	}

	if !cached {
		rolePermissionInsertCacheMut.Lock()
		rolePermissionInsertCache[key] = cache
		rolePermissionInsertCacheMut.Unlock()
	}

	return o.doAfterInsertHooks(ctx, exec)
}

// UpdateG a single RolePermission record using the global executor.
// See Update for more documentation.
func (o *RolePermission) UpdateG(ctx context.Context, columns boil.Columns) (int64, error) {
	return o.Update(ctx, boil.GetContextDB(), columns)
}

// Update uses an executor to update the RolePermission.
// See boil.Columns.UpdateColumnSet documentation to understand column list inference for updates.
// Update does not automatically update the record in case of default values. Use .Reload() to refresh the records.
func (o *RolePermission) Update(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) (int64, error) {
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		o.UpdatedAt = currTime
	}

	var err error
	if err = o.doBeforeUpdateHooks(ctx, exec); err != nil {
		return 0, err
	}
	key := makeCacheKey(columns, nil)
	rolePermissionUpdateCacheMut.RLock()
	cache, cached := rolePermissionUpdateCache[key]
	rolePermissionUpdateCacheMut.RUnlock()

	if !cached {
		wl := columns.UpdateColumnSet(
			rolePermissionAllColumns,
			rolePermissionPrimaryKeyColumns,
		)

		if !columns.IsWhitelist() {
			wl = strmangle.SetComplement(wl, []string{"created_at"})
		}
		if len(wl) == 0 {
			return 0, errors.New("models: unable to update role_permission, could not build whitelist")
		}

		cache.query = fmt.Sprintf("UPDATE \"role_permission\" SET %s WHERE %s",
			strmangle.SetParamNames("\"", "\"", 1, wl),
			strmangle.WhereClause("\"", "\"", len(wl)+1, rolePermissionPrimaryKeyColumns),
		)
		cache.valueMapping, err = queries.BindMapping(rolePermissionType, rolePermissionMapping, append(wl, rolePermissionPrimaryKeyColumns...))
		if err != nil {
			return 0, err
		}
	}

	values := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), cache.valueMapping)

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, values)
	}
	var result sql.Result
	result, err = exec.ExecContext(ctx, cache.query, values...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update role_permission row")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by update for role_permission")
	}

	if !cached {
		rolePermissionUpdateCacheMut.Lock()
		rolePermissionUpdateCache[key] = cache
		rolePermissionUpdateCacheMut.Unlock()
	}

	return rowsAff, o.doAfterUpdateHooks(ctx, exec)
}

// UpdateAllG updates all rows with the specified column values.
func (q rolePermissionQuery) UpdateAllG(ctx context.Context, cols M) (int64, error) {
	return q.UpdateAll(ctx, boil.GetContextDB(), cols)
}

// UpdateAll updates all rows with the specified column values.
func (q rolePermissionQuery) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	queries.SetUpdate(q.Query, cols)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all for role_permission")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected for role_permission")
	}

	return rowsAff, nil
}

// UpdateAllG updates all rows with the specified column values.
func (o RolePermissionSlice) UpdateAllG(ctx context.Context, cols M) (int64, error) {
	return o.UpdateAll(ctx, boil.GetContextDB(), cols)
}

// UpdateAll updates all rows with the specified column values, using an executor.
func (o RolePermissionSlice) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	ln := int64(len(o))
	if ln == 0 {
		return 0, nil
	}

	if len(cols) == 0 {
		return 0, errors.New("models: update all requires at least one column argument")
	}

	colNames := make([]string, len(cols))
	args := make([]interface{}, len(cols))

	i := 0
	for name, value := range cols {
		colNames[i] = name
		args[i] = value
		i++
	}

	// Append all of the primary key values for each column
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), rolePermissionPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := fmt.Sprintf("UPDATE \"role_permission\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 1, colNames),
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), len(colNames)+1, rolePermissionPrimaryKeyColumns, len(o)))

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args...)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all in rolePermission slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected all in update all rolePermission")
	}
	return rowsAff, nil
}

// UpsertG attempts an insert, and does an update or ignore on conflict.
func (o *RolePermission) UpsertG(ctx context.Context, updateOnConflict bool, conflictColumns []string, updateColumns, insertColumns boil.Columns, opts ...UpsertOptionFunc) error {
	return o.Upsert(ctx, boil.GetContextDB(), updateOnConflict, conflictColumns, updateColumns, insertColumns, opts...)
}

// Upsert attempts an insert using an executor, and does an update or ignore on conflict.
// See boil.Columns documentation for how to properly use updateColumns and insertColumns.
func (o *RolePermission) Upsert(ctx context.Context, exec boil.ContextExecutor, updateOnConflict bool, conflictColumns []string, updateColumns, insertColumns boil.Columns, opts ...UpsertOptionFunc) error {
	if o == nil {
		return errors.New("models: no role_permission provided for upsert")
	}
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		if o.CreatedAt.IsZero() {
			o.CreatedAt = currTime
		}
		o.UpdatedAt = currTime
	}

	if err := o.doBeforeUpsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(rolePermissionColumnsWithDefault, o)

	// Build cache key in-line uglily - mysql vs psql problems
	buf := strmangle.GetBuffer()
	if updateOnConflict {
		buf.WriteByte('t')
	} else {
		buf.WriteByte('f')
	}
	buf.WriteByte('.')
	for _, c := range conflictColumns {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(updateColumns.Kind))
	for _, c := range updateColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(insertColumns.Kind))
	for _, c := range insertColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	for _, c := range nzDefaults {
		buf.WriteString(c)
	}
	key := buf.String()
	strmangle.PutBuffer(buf)

	rolePermissionUpsertCacheMut.RLock()
	cache, cached := rolePermissionUpsertCache[key]
	rolePermissionUpsertCacheMut.RUnlock()

	var err error

	if !cached {
		insert, _ := insertColumns.InsertColumnSet(
			rolePermissionAllColumns,
			rolePermissionColumnsWithDefault,
			rolePermissionColumnsWithoutDefault,
			nzDefaults,
		)

		update := updateColumns.UpdateColumnSet(
			rolePermissionAllColumns,
			rolePermissionPrimaryKeyColumns,
		)

		if updateOnConflict && len(update) == 0 {
			return errors.New("models: unable to upsert role_permission, could not build update column list")
		}

		ret := strmangle.SetComplement(rolePermissionAllColumns, strmangle.SetIntersect(insert, update))

		conflict := conflictColumns
		if len(conflict) == 0 && updateOnConflict && len(update) != 0 {
			if len(rolePermissionPrimaryKeyColumns) == 0 {
				return errors.New("models: unable to upsert role_permission, could not build conflict column list")
			}

			conflict = make([]string, len(rolePermissionPrimaryKeyColumns))
			copy(conflict, rolePermissionPrimaryKeyColumns)
		}
		cache.query = buildUpsertQueryPostgres(dialect, "\"role_permission\"", updateOnConflict, ret, update, conflict, insert, opts...)

		cache.valueMapping, err = queries.BindMapping(rolePermissionType, rolePermissionMapping, insert)
		if err != nil {
			return err
		}
		if len(ret) != 0 {
			cache.retMapping, err = queries.BindMapping(rolePermissionType, rolePermissionMapping, ret)
			if err != nil {
				return err
			}
		}
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)
	var returns []interface{}
	if len(cache.retMapping) != 0 {
		returns = queries.PtrsFromMapping(value, cache.retMapping)
	}

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, vals)
	}
	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(returns...)
		if errors.Is(err, sql.ErrNoRows) {
			err = nil // Postgres doesn't return anything when there's no update
		}
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}
	if err != nil {
		return errors.Wrap(err, "models: unable to upsert role_permission")
	}

	if !cached {
		rolePermissionUpsertCacheMut.Lock()
		rolePermissionUpsertCache[key] = cache
		rolePermissionUpsertCacheMut.Unlock()
	}

	return o.doAfterUpsertHooks(ctx, exec)
}

// DeleteG deletes a single RolePermission record.
// DeleteG will match against the primary key column to find the record to delete.
func (o *RolePermission) DeleteG(ctx context.Context) (int64, error) {
	return o.Delete(ctx, boil.GetContextDB())
}

// Delete deletes a single RolePermission record with an executor.
// Delete will match against the primary key column to find the record to delete.
func (o *RolePermission) Delete(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no RolePermission provided for delete")
	}

	if err := o.doBeforeDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	args := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), rolePermissionPrimaryKeyMapping)
	sql := "DELETE FROM \"role_permission\" WHERE \"role_id\"=$1 AND \"permission_id\"=$2 AND \"id\"=$3"

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args...)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete from role_permission")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by delete for role_permission")
	}

	if err := o.doAfterDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	return rowsAff, nil
}

func (q rolePermissionQuery) DeleteAllG(ctx context.Context) (int64, error) {
	return q.DeleteAll(ctx, boil.GetContextDB())
}

// DeleteAll deletes all matching rows.
func (q rolePermissionQuery) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if q.Query == nil {
		return 0, errors.New("models: no rolePermissionQuery provided for delete all")
	}

	queries.SetDelete(q.Query)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from role_permission")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for role_permission")
	}

	return rowsAff, nil
}

// DeleteAllG deletes all rows in the slice.
func (o RolePermissionSlice) DeleteAllG(ctx context.Context) (int64, error) {
	return o.DeleteAll(ctx, boil.GetContextDB())
}

// DeleteAll deletes all rows in the slice, using an executor.
func (o RolePermissionSlice) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if len(o) == 0 {
		return 0, nil
	}

	if len(rolePermissionBeforeDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doBeforeDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	var args []interface{}
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), rolePermissionPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "DELETE FROM \"role_permission\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, rolePermissionPrimaryKeyColumns, len(o))

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from rolePermission slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for role_permission")
	}

	if len(rolePermissionAfterDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	return rowsAff, nil
}

// ReloadG refetches the object from the database using the primary keys.
func (o *RolePermission) ReloadG(ctx context.Context) error {
	if o == nil {
		return errors.New("models: no RolePermission provided for reload")
	}

	return o.Reload(ctx, boil.GetContextDB())
}

// Reload refetches the object from the database
// using the primary keys with an executor.
func (o *RolePermission) Reload(ctx context.Context, exec boil.ContextExecutor) error {
	ret, err := FindRolePermission(ctx, exec, o.RoleID, o.PermissionID, o.ID)
	if err != nil {
		return err
	}

	*o = *ret
	return nil
}

// ReloadAllG refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *RolePermissionSlice) ReloadAllG(ctx context.Context) error {
	if o == nil {
		return errors.New("models: empty RolePermissionSlice provided for reload all")
	}

	return o.ReloadAll(ctx, boil.GetContextDB())
}

// ReloadAll refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *RolePermissionSlice) ReloadAll(ctx context.Context, exec boil.ContextExecutor) error {
	if o == nil || len(*o) == 0 {
		return nil
	}

	slice := RolePermissionSlice{}
	var args []interface{}
	for _, obj := range *o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), rolePermissionPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "SELECT \"role_permission\".* FROM \"role_permission\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, rolePermissionPrimaryKeyColumns, len(*o))

	q := queries.Raw(sql, args...)

	err := q.Bind(ctx, exec, &slice)
	if err != nil {
		return errors.Wrap(err, "models: unable to reload all in RolePermissionSlice")
	}

	*o = slice

	return nil
}

// RolePermissionExistsG checks if the RolePermission row exists.
func RolePermissionExistsG(ctx context.Context, roleID string, permissionID string, iD string) (bool, error) {
	return RolePermissionExists(ctx, boil.GetContextDB(), roleID, permissionID, iD)
}

// RolePermissionExists checks if the RolePermission row exists.
func RolePermissionExists(ctx context.Context, exec boil.ContextExecutor, roleID string, permissionID string, iD string) (bool, error) {
	var exists bool
	sql := "select exists(select 1 from \"role_permission\" where \"role_id\"=$1 AND \"permission_id\"=$2 AND \"id\"=$3 limit 1)"

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, roleID, permissionID, iD)
	}
	row := exec.QueryRowContext(ctx, sql, roleID, permissionID, iD)

	err := row.Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "models: unable to check if role_permission exists")
	}

	return exists, nil
}

// Exists checks if the RolePermission row exists.
func (o *RolePermission) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	return RolePermissionExists(ctx, exec, o.RoleID, o.PermissionID, o.ID)
}
