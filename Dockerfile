FROM golang:1.21.4 AS build

WORKDIR /app

COPY . .

RUN go mod download

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main ./cmd/app/main.go

FROM alpine

WORKDIR /bin/app

COPY --from=build /app/ .

RUN mkdir -p static/files

