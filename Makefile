swagger:
	swag fmt && swag init -g ./internal/app/api.go
dev:
	make swagger && go run ./cmd/app/main.go
pull_db:
	sqlboiler psql --add-global-variants