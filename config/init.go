package config

import (
	"github.com/joho/godotenv"
	"log"
)

func Init() {
	if err := godotenv.Load(); err != nil {

		log.Println("No env file, using default environment")
	}
}
