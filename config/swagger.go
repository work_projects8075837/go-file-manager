package config

import (
	"github.com/gofiber/contrib/swagger"
)

var (
	SwaggerConfig = swagger.Config{
		BasePath: "/api-file",
		FilePath: "./docs/swagger.json",
		Path:     "./docs",
		Title:    "Swagger API Docs",
	}
)
