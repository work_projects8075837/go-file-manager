package config

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"os"
	"strconv"
)

var (
	StaticConfig = fiber.Static{
		CacheDuration: 3 * 60 * 60,
		Download:      true,
		Compress:      true,
		ByteRange:     true,
	}
)

func GetFiberConfig() fiber.Config {
	envPrefork, _ := strconv.Atoi(os.Getenv("PREFORK"))

	return fiber.Config{
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
		Prefork:     envPrefork != 0,
		BodyLimit:   1024 * 1024 * 1024,
		ProxyHeader: fiber.HeaderXForwardedFor,
	}
}
