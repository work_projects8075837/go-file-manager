package fileslib

import (
	"fmt"
	"github.com/nfnt/resize"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"os"
	"strings"
)

func CompressAndSaveImage(imageReader interface{ io.Reader }, filename string) error {
	if !IsImage(filename) {
		out, err := os.Create(fmt.Sprintf("./static/files/%s", filename))
		if err != nil {
			return err
		}
		defer out.Close()

		_, err = io.Copy(out, imageReader)
		if err != nil {
			return err
		}
	}
	file, _, err := image.Decode(imageReader)
	if err != nil {
		return err
	}

	bounds := file.Bounds()
	width := bounds.Max.X
	height := bounds.Max.Y

	const maxWidth = 1000
	const maxHeight = 800
	var newWidth, newHeight uint
	if width > height {
		newWidth = maxWidth
		newHeight = uint(float64(height) * (float64(maxWidth) / float64(width)))
	} else {
		newWidth = uint(float64(width) * (float64(maxHeight) / float64(height)))
		newHeight = maxHeight
	}

	resizedImage := resize.Resize(newWidth, newHeight, file, resize.Lanczos3)

	out, err := os.Create(fmt.Sprintf("./static/files/%s", filename))
	if err != nil {
		return err
	}
	defer out.Close()

	ext := GetImageExt(filename)
	if ext == "png" {
		png.Encode(out, resizedImage)
	} else {
		jpeg.Encode(out, resizedImage, &jpeg.Options{Quality: 80})

	}

	return nil
}

func IsImage(filename string) bool {
	ext := GetImageExt(filename)

	imageExtensions := []string{"jpg", "jpeg", "png"}

	for _, imageExt := range imageExtensions {
		if ext == imageExt {
			return true
		}
	}

	return false
}

func GetImageExt(filename string) string {
	splitFilename := strings.Split(filename, ".")
	ext := splitFilename[len(splitFilename)-1]

	return ext
}
